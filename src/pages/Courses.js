import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";
import {useState, useEffect} from "react";

export default function Courses(){

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch("https://guarded-woodland-31326.herokuapp.com/products/ActiveProducts")
		.then(res => res.json())
		.then(data => {
			console.log(data);

		setCourses(data.map(course => {
			return (
				<CourseCard key={course._id} courseProp = {course}/>
			)
		}));
		});

	}, []);

	return(

	<>
		<h1>Available Products</h1>
		{courses}
	</>
	)

}