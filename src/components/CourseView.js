import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CourseView(){

	const {user} = useContext(UserContext);

	// allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const history = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	// useParams hook allows use to retrieve the courseId passed via the URL
	const {courseId} = useParams();

	const enroll = (courseId) => {

		fetch('https://guarded-woodland-31326.herokuapp.com/orders/addOrder', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: 'Purchased!',
					icon: 'success',
					text: 'Your pet is waiting'
				});

				history("/courses");

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				});
			};

		});
	};

	useEffect(() => {
		console.log(courseId)
		fetch(`https://guarded-woodland-31326.herokuapp.com/products/getSingleProduct/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});

	}, [courseId]);

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:4}}>
					
					<Card style={{ width: '18rem' }}>
					      <Card.Img variant="top" src="https://media.istockphoto.com/photos/toys-for-dogs-isolated-picture-id1316829099?b=1&k=20&m=1316829099&s=170667a&w=0&h=atLJanAvHDI8xCKTbMPP6STDHKkFQM3oAqyzarSXOdg=" />
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price {price}</Card.Subtitle>
					        
					        { user.id !== null ?
					        	<Button variant="primary" onClick={() => enroll(courseId)}>Purchase</Button>
					        	:
					        	<Link className="btn btn-danger" to="/login">Log In</Link>
					        }
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>

	)
}
