import {useState} from "react";
import {Card, Button}        from 'react-bootstrap';
// import {Link}                from 'react-router-dom';
import Modal                 from 'react-bootstrap/Modal';
// import {useParams}           from 'react-router-dom';
import Form                  from 'react-bootstrap/Form';


export default function ProductCard({productProp}){
	const {name, description, price, _id, isActive} = productProp;
    const [show, setShow]                           = useState(false);
  	const handleShow                                = () => setShow(true);
  	const handleClose                               = () => setShow(false);
  	const [newname, setName1]              			= useState();
	const [newdescription ,setDescription1] 		= useState();
	const [newprice, setPrice1]            			= useState();
  


	//deleteProduct Function
	const deleteProduct = (token) => {
		console.log('at delte');
	  fetch(`https://guarded-woodland-31326.herokuapp.com/products/deleteProduct/${_id}`,{
	    method : 'DELETE',
	    headers: {
	      Authorization  : `Bearer ${localStorage.getItem('token')}`,
	      'Content-type' : 'application/json'
	    }
	  })
	  .then(res => res.json())
	  .then(data => {
	    //TODO:TESTING
	    console.log(data);
	  })
	};
	//end deleteProduction function ////////////////////

	const archiveProduct = (token) => {
	  fetch(`https://guarded-woodland-31326.herokuapp.com/products/archiveProduct/${_id}`,{
	    method : 'PUT',
	    headers: {
	      Authorization  : `Bearer ${localStorage.getItem('token')}`,
	      'Content-type' : 'application/json'
	    }
	  })
	  .then(res => res.json())
	  .then(data => {
	    //TODO:TESTING
	    console.log(data);
	  })
	};

	//activateProduct //////////////////////////////
	const activateProduct = (token) => {
	  fetch(`https://guarded-woodland-31326.herokuapp.com/products/activateProduct/${_id}`,{
	    method : 'PUT',
	    headers: {
	      Authorization  : `Bearer ${localStorage.getItem('token')}`,
	      'Content-type' : 'application/json'
	    }
	  })
	  .then(res => res.json())
	  .then(data => {
	    //TODO:TESTING
	    console.log(data);
	  })
	};

	//update product function //////////////////////////
	const updateProduct = (token) => {
	  //TODO:TESTING
	  console.log('here');
	  console.log(name);
	  console.log(description);
	  console.log(_id);
	  fetch(`https://guarded-woodland-31326.herokuapp.com/products/updateProduct/${_id}`,{
	    method : 'PUT',
	    headers: {
	      Authorization  : `Bearer ${localStorage.getItem('token')}`,
	      'Content-type' : 'application/json'
	    },
	    body : JSON.stringify({
	      name       : newname,
	      description: newdescription,
	      price      : newprice
	    })
	  })
	  .then(res => res.json())
	  .then(data => {
	    //TODO:TESTING
	    console.log(data);

	  })
	};
	//end updateProduct function

	return (
		<>
			<Card className="CourseCard p-3 mb-3 w-50">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
				</Card.Body>

				{
					(isActive)?
					<>
						<Button variant="primary" className="w-50 mb-2">Edit</Button>
						<Button variant="danger" className="w-50 mb-2" onClick={deleteProduct}>Delete</Button>
						<Button variant="primary" className="w-50 mb-2" onClick={archiveProduct}>Archive</Button>
					</>
					:
					<>
						<Button variant="primary" className="w-50 mb-2" onClick={handleShow}>Edit</Button>
						<Button variant="danger" className="w-50 mb-2">Delete</Button>
						<Button variant="secondary" className="w-50 mb-2" onClick={activateProduct}>Set as active</Button>
					</>
				}
			</Card>
			
			<Modal show={show} onHide={handleClose}>
		        <Modal.Header closeButton>
		          <Modal.Title>Add/Edit Product</Modal.Title>
		        </Modal.Header>
		        <Modal.Body>
		          <Form>
		            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
		              <Form.Label>Product Name</Form.Label>
		              <Form.Control 
		                type="text" 
		                placeholder="Enter product name" 
		                required
		                defaultValue={name}
		                onChange={(e) => setName1(e.target.value)}/>
		            </Form.Group>
		            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
		              <Form.Label>Price</Form.Label>
		              <Form.Control
		                type="number" 
		                placeholder="Enter Price" 
		                required
		                defaultValue={price}
		                onChange={(e) => setPrice1(e.target.value)} />
		            </Form.Group>
		            <Form.Group
		              className="mb-3"
		              controlId="exampleForm.ControlTextarea1">
		              <Form.Label>Description</Form.Label>
		              <Form.Control as="textarea" rows={3} 
		                type="number" 
		                placeholder="Description" 
		                required
		                defaultValue={description}
		                onChange={(e) => setDescription1(e.target.value)} />
		            </Form.Group>
		          </Form>
		        </Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>Close</Button>
		          <Button variant="primary" onClick={updateProduct}>Update Product</Button>
		        </Modal.Footer>
	      	</Modal>
	    </>		
	)
}